/**
 *******************************************************************************
 * @file                  WordChainTestSuite.java
 * @version               1.1
 * @date                  18.10.2017
 * @author                devi7
 *
 * @brief                 Word chain Test Suite
 * @owner                 devi7
 *******************************************************************************/

import java.util.*;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class WordChainTestSuite {

    @org.junit.Test
    public void generateWordChainTestCase0() throws Exception {
        String[] dictionary = new String[]{};
        String startWord = "";
        String stopWord = "";
        WordChain wordChain = new WordChain();
        List<String> wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        List<String> expected = Collections.emptyList();
        assertThat(wordChainPath, is(expected));
    }

    @org.junit.Test
    public void generateWordChainTestCase1() throws Exception {
        String[] dictionary = new String[]{"a"};
        String startWord = "";
        String stopWord = "";
        WordChain wordChain = new WordChain();
        List<String> wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        List<String> expected = Collections.emptyList();
        assertThat(wordChainPath, is(expected));

        startWord = "a";
        wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        assertThat(wordChainPath, is(expected));
    }

    @org.junit.Test
    public void generateWordChainTestCase2() throws Exception {
        String[] dictionary = new String[]{"a", "a"};
        String startWord = "a";
        String stopWord = "";
        WordChain wordChain = new WordChain();
        List<String> wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        List<String> expected = Collections.emptyList();
        assertThat(wordChainPath, is(expected));

        stopWord = "a";
        wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        expected = Arrays.asList("a", "a");
        assertThat(wordChainPath, is(expected));
    }

    @org.junit.Test
    public void generateWordChainTestCase3() throws Exception {
        String[] dictionary = new String[]{"a", "b"};
        String startWord = "a";
        String stopWord = "a";
        WordChain wordChain = new WordChain();
        List<String> wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        List<String> expected = Collections.emptyList();
        assertThat(wordChainPath, is(expected));

        stopWord = "b";
        wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        expected = Arrays.asList("a", "b");
        assertThat(wordChainPath, is(expected));
    }

    @org.junit.Test
    public void generateWordChainTestCase4() throws Exception {
        String[] dictionary = new String[]{"b", "a"};
        String startWord = "a";
        String stopWord = "b";
        WordChain wordChain = new WordChain();
        List<String> wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        List<String> expected = Arrays.asList("a", "b");
        assertThat(wordChainPath, is(expected));
    }

    @org.junit.Test
    public void generateWordChainTestCase5() throws Exception {
        String[] dictionary = new String[]{"a", "c", "b"};
        String startWord = "a";
        String stopWord = "b";
        WordChain wordChain = new WordChain();
        List<String> wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        List<String> expected = Arrays.asList("a", "b");
        assertThat(wordChainPath, is(expected));
    }

    @org.junit.Test
    public void generateWordChainTestCase6() throws Exception {
        String[] dictionary = new String[]{"aa", ""};
        String startWord = "";
        String stopWord = "";
        WordChain wordChain = new WordChain();
        List<String> wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        List<String> expected = Collections.emptyList();
        assertThat(wordChainPath, is(expected));

        startWord = "aa";
        wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        assertThat(wordChainPath, is(expected));

        startWord = "ab";
        wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        assertThat(wordChainPath, is(expected));
    }

    @org.junit.Test
    public void generateWordChainTestCase7() throws Exception {
        String[] dictionary = new String[]{"aa", "ab"};
        String startWord = "aa";
        String stopWord = "";
        WordChain wordChain = new WordChain();
        List<String> wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        List<String> expected = Collections.emptyList();
        assertThat(wordChainPath, is(expected));

        stopWord = "ab";
        wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        expected = Arrays.asList("aa", "ab");
        assertThat(wordChainPath, is(expected));
    }

    @org.junit.Test
    public void generateWordChainTestCase8() throws Exception {
        String[] dictionary = new String[]{"aa", "cc", "ab"};
        String startWord = "aa";
        String stopWord = "ab";
        WordChain wordChain = new WordChain();
        List<String> wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        List<String> expected = Arrays.asList("aa", "ab");
        assertThat(wordChainPath, is(expected));
    }

    @org.junit.Test
    public void generateWordChainTestCase9() throws Exception {
        String[] dictionary = new String[]{"aa", "aa", "ab"};
        String startWord = "aa";
        String stopWord = "ab";
        WordChain wordChain = new WordChain();
        List<String> wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        List<String> expected = Arrays.asList("aa", "ab");
        assertThat(wordChainPath, is(expected));

        stopWord = "aaa";
        wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        expected = Collections.emptyList();
        assertThat(wordChainPath, is(expected));

        startWord = "aaa";
        stopWord = "ab";
        wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        expected = Collections.emptyList();
        assertThat(wordChainPath, is(expected));
    }

    @org.junit.Test
    public void generateWordChainTestCase10() throws Exception {
        String[] dictionary = new String[]{"aa", "aaa", "ab"};
        String startWord = "aa";
        String stopWord = "aaa";
        WordChain wordChain = new WordChain();
        List<String> wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        List<String> expected = Collections.emptyList();
        assertThat(wordChainPath, is(expected));
    }

    @org.junit.Test
    public void generateWordChainTestCase11() throws Exception {
        String[] dictionary = new String[]{"dag", "dog", "cat", "bat", "dat"};
        String startWord = "cat";
        String stopWord = "dog";
        WordChain wordChain = new WordChain();
        List<String> wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        List<String> expected = Arrays.asList("cat", "dat", "dag", "dog");
        assertThat(wordChainPath, is(expected));
    }

    @org.junit.Test
    public void generateWordChainTestCase12() throws Exception {
        String[] dictionary = new String[]{"car", "cat", "lead", "larp", "load", "barn", "bold", "bild", "lags", "boss", "leap", "gold", "goad"};
        String startWord = "lead";
        String stopWord = "gold";
        WordChain wordChain = new WordChain();
        List<String> wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        List<String> expected = Arrays.asList("lead", "load", "goad", "gold");
        assertThat(wordChainPath, is(expected));
    }

    @org.junit.Test
    public void generateWordChainTestCase13() throws Exception {
        String[] dictionary = new String[]{
                "lead",
                "larp",
                "load",
                "barn",
                "lags",
                "boss",
                "leap",
                "A",
                "A'asia",
                "A's",
                "AA",
                "AA's",
                "AAA",
                "AAM",
                "AB",
                "AB's",
                "ABA",
                "ABC",
                "ABC's",
                "ABCs",
                "ABD",
                "ABDs",
                "ABM",
                "ABM's",
                "ABMs",
                "ABS",
                "AC",
                "AC's",
                "ACLU",
                "ACT",
                "ACTH",
                "zzza",
                "ACTH's",
                "AD",
                "AD's",
                "ADC",
                "ADD",
                "ADP",
                "aaaaa",
                "ADP's",
                "ADR",
                "ADRs",
                "AEC",
                "AEC's",
                "AF",
                "AFAIK",
                "AFAIKs",
                "AFB",
                "AFC",
                "AFDC",
                "AFN",
                "AFT",
                "AGC",
                "AHQ",
                "AI",
                "AI's",
                "AIDS",
                "AIDS's",
                "AIDSes",
                "AIs",
                "AK",
                "AL",
                "ALGOL",
                "ALGOL's",
                "ALU",
                "AM",
                "AM's",
                "AMA",
                "AMP",
                "AMP's",
                "AND",
                "ANSI",
                "ANSIs",
                "ANTU",
                "ANZUS",
                "AOL",
                "AOL's",
                "AP",
                "APB",
                "APC",
                "APC's",
                "APO",
                "APR",
                "AQ",
                "AR",
                "ARC",
                "AS",
                "ASAP",
                "ASAT",
                "ASCII",
                "ASCII's",
                "ASCIIs",
                "ASDIC",
                "ASL",
                "ASL's",
                "ASPCA",
                "ASSR",
                "ATC",
                "ATM",
                "ATM's",
                "ATP",
                "ATP's",
                "ATPase",
                "ATPases",
                "ATS",
                "ATV",
                "ATVs",
                "AV",
                "AWACS",
                "AWOL",
                "AWOL's",
                "AZ",
                "AZ's",
                "AZT",
                "AZT's",
                "Aachen",
                "Aalborg",
                "Aalesund",
                "Aaliyah",
                "Aaliyah's",
                "Aalst",
                "Aalto",
                "Aarau",
                "Aargau",
                "Aarhus",
                "Aaron",
                "Aaronic",
                "Aaronical",
                "Aaronsburg",
                "Aaronsburg's",
                "Ab",
                "Ab's",
                "Abadan",
                "Abaddon",
                "Abba",
                "Abbado",
                "Abbado's",
                "Abbas",
                "Abbasid",
                "Abbasids",
                "Abbeville",
                "Abbeville's",
                "Abbevillean",
                "Abbevillian",
                "Abbotsford",
                "Abbotsford's",
                "Abbott",
                "Abbott's",
                "Abbottstown",
                "Abbottstown's",
                "Abby",
                "Abby's",
                "Abbyville",
                "Abbyville's",
                "Abderian",
                "Abderian's",
                "Abderite",
                "Abderite's",
                "Abdias",
                "Abdul",
                "Abdul's",
                "Abdullah",
                "Abdullah's",
                "Abe",
                "Abe's",
                "Abednego",
                "Abel",
                "Abel's",
                "Abelard",
                "Abelian",
                "Abell",
                "Abell's",
                "Abelson",
                "Abelson's",
                "Abenaki",
                "Abenakis",
                "Abeokuta",
                "Abercrombie",
                "Abercrombie's",
                "Aberdare",
                "Aberdeen",
                "Aberdeen's",
                "Aberdeenshire",
                "Aberdeenshire's",
                "Aberdonian",
                "Aberdonians",
                "Abernant",
                "Abernant's",
                "Abernathy",
                "Abernathy's",
                "Aberystwyth",
                "Aberystwyth's",
                "Abib",
                "Abibs",
                "Abidjan",
                "Abidjan's",
                "Abigail",
                "Abilene",
                "Abingdon",
                "Abington",
                "Abington's",
                "Abiquiu",
                "Abiquiu's",
                "Abkhas",
                "zaaa",
                "Abkhases",
                "Abkhasian",
                "Abkhasians",
                "Abkhaz",
                "Abkhazes",
                "Abkhazia",
                "Abkhazia's",
                "Abkhazian",
                "Abkhazians",
                "Abner",
                "Abner's",
                "Abo",
                "Abo's",
                "Aboriginal",
                "Aboriginal's",
                "Aboriginals",
                "Aborigine",
                "Aborigine's",
                "Aborigines",
                "Abos",
                "Abraham",
                "Abraham's",
                "Abram",
                "Abram's",
                "Abrams",
                "Abroma",
                "Abroma's",
                "Abrus",
                "Abrus's",
                "Abruzzi",
                "Abruzzi's",
                "Abs",
                "Absalom",
                "Absaraka",
                "Absaraka's",
                "Absaroke",
                "Absarokee",
                "Absarokee's",
                "Absarokes",
                "Absecon",
                "Absecon's",
                "Abuja",
                "Abukir",
                "Abydos",
                "Abyssinia",
                "Abyssinia's",
                "Abyssinian",
                "Abyssinian's",
                "Abyssinians",
                "Ac",
                "Ac's",
                "Acadia",
                "Acadia's",
                "Acadian",
                "Acadian's",
                "Acadians",
                "Acalepha",
                "Acalepha's",
                "Acalephae",
                "Acalephae's",
                "dat",
                "Acampo",
                "Acampo's",
                "Acanthaceae",
                "Acanthaceae's",
                "zzzz",
                "Acanthocephala",
                "Acanthocephala's",
                "Acapulco",
                "Acapulco's",
                "Acarida",
                "Acarida's",
                "Acarina",
                "Acarina's",
                "Acarnanian",
                "Acarnanians",
                "zzaa",
                "Accad",
                "cat",
                "Accadian",
                "Accadians",
                "cad",
                "Accenture",
                "Accenture's",
                "gag",
                "Accokeek",
                "Accokeek's",
                "Accolate",
                "Accolates",
                "dog",
                "Accomac",
                "Accomac's",
                "dag"};
        String startWord = "aaaa";
        String stopWord = "zzzz";
        WordChain wordChain = new WordChain();
        List<String> wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        List<String> expected = Arrays.asList("aaaa", "zaaa", "zzaa", "zzza", "zzzz");
        assertThat(wordChainPath, is(expected));

        startWord = "cat";
        stopWord = "dog";
        wordChainPath = wordChain.generateWordChain(startWord, stopWord, dictionary);
        expected = Arrays.asList("cat", "dat", "dag", "dog");
        assertThat(wordChainPath, is(expected));
    }

    @org.junit.Test
    public void updateWordChainGraphTestCase() throws Exception {
        WordChain wordChain = new WordChain();
        List<List<String>> wordChainGraph = new ArrayList<>();
        String word = "cat";
        wordChain.updateWordChainGraph(wordChainGraph, word);
        List<List<String>> expected = new ArrayList<>();
        expected.add(new ArrayList<>());
        expected.get(0).add("cat");
        assertThat(wordChainGraph, is(expected));
    }

    @org.junit.Test
    public void checkIfOnlyOneLetterIsDifferentTestCase() throws Exception {
        WordChain wordChain = new WordChain();
        String a = "cat";
        String b = "dat";
        assertTrue(wordChain.checkIfOnlyOneLetterIsDifferent(a, b));
        a = "cat";
        b = "dog";
        assertFalse(wordChain.checkIfOnlyOneLetterIsDifferent(a, b));
    }

    @org.junit.Test
    public void checkIfInVerticalListTestCase() throws Exception {
        WordChain wordChain = new WordChain();
        List<List<String>> wordChainGraph = new ArrayList<>();
        String word = "cat";
        wordChainGraph.add(new ArrayList<>());
        wordChainGraph.get(0).add(word);
        assertTrue(wordChain.checkIfInVerticalList(wordChainGraph, word));
        word = "dog";
        assertFalse(wordChain.checkIfInVerticalList(wordChainGraph, word));
    }

    @org.junit.Test
    public void generateWordChainGraphTestCase() throws Exception {
        WordChain wordChain = new WordChain();
        String startWord = "cat";
        String[] dictionary = new String[]{"cat", "cot", "dow", "kat", "cog", "dog"};
        List<List<String>> wordChainGraph = wordChain.generateWordChainGraph(startWord, dictionary);
        List<List<String>> expected = new ArrayList<>();
        expected.add(new ArrayList<>());
        expected.get(0).add("cat");
        expected.get(0).add("cot");
        expected.get(0).add("kat");
        expected.add(new ArrayList<>());
        expected.get(1).add("cot");
        expected.get(1).add("cog");
        expected.add(new ArrayList<>());
        expected.get(2).add("kat");
        expected.add(new ArrayList<>());
        expected.get(3).add("cog");
        expected.get(3).add("dog");
        expected.add(new ArrayList<>());
        expected.get(4).add("dog");
        expected.get(4).add("dow");
        expected.add(new ArrayList<>());
        expected.get(5).add("dow");
        assertThat(wordChainGraph, is(expected));
    }
}
