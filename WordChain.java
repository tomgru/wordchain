/**
 *******************************************************************************
 * @file                  WordChain.java
 * @version               1.1
 * @date                  18.10.2017
 * @author                devi7
 *
 * @brief                 Word chain generator
 * @owner                 devi7
 *
 * @description
 * Initially a simple graph is built based on the neighborhood list.
 * Then, based on modified DFS, the path is searched between startWord and stopWord.
 *******************************************************************************/

import java.util.Objects;
import java.util.List;
import java.util.ArrayList;
import java.util.Stack;

class WordChain {

    void updateWordChainGraph(List<List<String>> wordChainGraph, String word) {
        List neighbors = new ArrayList<>();
        neighbors.add(word);
        wordChainGraph.add(neighbors);
    }

    boolean checkIfOnlyOneLetterIsDifferent(String a, String b) {
        if (a.length() != b.length()) {
            return false;
        }
        int nrOfDiffLetters = 0;
        int aSize = a.length();
        for (int i = 0; i < aSize; ++i) {
            if (nrOfDiffLetters < 2 && a.charAt(i) != b.charAt(i)) {
                ++nrOfDiffLetters;
            }
        }
        return nrOfDiffLetters == 1;
    }

    boolean checkIfInVerticalList(List<List<String>> listOfLists, String s) {
        if (listOfLists.size() == 0 || s.length() == 0) return false;
        for (List<String> horizontalList : listOfLists) {
            if (Objects.equals(horizontalList.get(0), s)) return true;
        }
        return false;
    }

    List<List<String>> generateWordChainGraph(String startWord, String[] dictionary) {
        List<List<String>> wordChainGraph = new ArrayList<>();
        updateWordChainGraph(wordChainGraph, startWord);
        int curGraphNodeIdx = 0;
        String curStartWord;
        while (curGraphNodeIdx < wordChainGraph.size()) {
            curStartWord = wordChainGraph.get(curGraphNodeIdx).get(0);
            for (String word : dictionary) {
                if (checkIfOnlyOneLetterIsDifferent(word, curStartWord)
                        && !checkIfInVerticalList(wordChainGraph, word)) {
                    wordChainGraph.get(curGraphNodeIdx).add(word);
                    updateWordChainGraph(wordChainGraph, word);
                }
            }
            ++curGraphNodeIdx;
        }
        return wordChainGraph;
    }

    private boolean updatePathAndHelperStack(List<List<String>> wordChainGraph, String startWord, String stopWord, List<String> path, List<String> dfsHelperStack) {
        for (List<String> horizontalList : wordChainGraph) {
            if (Objects.equals(horizontalList.get(0), startWord)) {
                if (horizontalList.size() == 1 && Objects.equals(startWord, stopWord)) return true;
                if (horizontalList.size() == 1 && path.size() > 0) path.remove(path.size() - 1);
                else {
                    for (int j = 1; j < horizontalList.size(); ++j) {
                        if (horizontalList.get(j).equals(stopWord)) return true;
                        dfsHelperStack.add(horizontalList.get(j));
                    }
                }
                break;
            }
        }
        return false;
    }

    private List<String> generateWordChainPathByDfs(String startWord, String stopWord, List<List<String>> wordChainGraph) {
        List<String> path = new ArrayList<>();
        Stack dfsHelperStack = new Stack();
        List<String> visitedNodes = new ArrayList<>();
        String top;
        while (true) {
            if (updatePathAndHelperStack(wordChainGraph, startWord, stopWord, path, dfsHelperStack)) {
                path.add(stopWord);
                return path;
            }
            if (dfsHelperStack.size() == 0) return path;
            top = (String) dfsHelperStack.pop();
            while (visitedNodes.contains(top) && dfsHelperStack.size() > 0) {
                top = (String) dfsHelperStack.pop();
            }
            path.add(top);
            startWord = top;
            visitedNodes.add(top);
        }
    }

    List<String> generateWordChain(String startWord, String stopWord, String[] dictionary) {
        if (startWord.length() == 0 || stopWord.length() == 0 || startWord.length() != stopWord.length() || dictionary.length < 2)
            return new ArrayList<>();
        List<List<String>> wordChainGraph = generateWordChainGraph(startWord, dictionary);
        List<String> path = generateWordChainPathByDfs(startWord, stopWord, wordChainGraph);
        int pathSize = path.size();
        if (pathSize > 0 && Objects.equals(path.get(pathSize - 1), stopWord)) {
            path.add(0, startWord);
            return path;
        }
        return new ArrayList<>();
    }
}
